FROM docker:19.03.13

# Installs latest Chromium package.
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" > /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && echo "http://dl-cdn.alpinelinux.org/alpine/v3.12/main" >> /etc/apk/repositories \
    && apk upgrade -a

# APK
RUN apk add \
bash \
curl \
git \
jq \
nodejs \
nodejs-npm \
openssh \
py-pip \
libtool \
build-base \
make \
autoconf \
automake \
shadow \
openssl \
ca-certificates \
bash

RUN ln -sf $(which python3) /usr/bin/python

# Pip
RUN python3 -m venv /opt/venv
RUN . /opt/venv/bin/activate && pip install \
anybadge

# kubernetes helm
ENV KUBERNETES_VERSION="v1.25.2"
# Note: Latest version of helm may be found at
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v3.3.4"

RUN curl -k -L https://dl.k8s.io/release/${KUBERNETES_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

RUN curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && chmod g+rwx /root \
    && mkdir /config \
    && chmod g+rwx /config

# Yq
RUN wget -q -O /usr/bin/yq $(wget -q -O - https://api.github.com/repos/mikefarah/yq/releases/latest | \
    jq -r '.assets[] | select(.name == "yq_linux_amd64") | .browser_download_url') \
    && chmod +x /usr/bin/yq

# Nodejs setup
USER root
RUN mkdir -p /home/node/.npm-global
RUN addgroup -S node && adduser -S node -G node
RUN chown node:node /home/node/.npm-global
ENV PATH=/home/node/.npm-global/bin:$PATH
ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

USER node

# NPM
RUN npm i -g \
typescript \
yarn \
plagiarize@0.0.55 \
expo-cli \
serverless

USER root

RUN apk add --no-cache \
    libstdc++ \
    chromium \
    harfbuzz \
    nss \
    freetype \
    ttf-freefont \
    wqy-zenhei \
    && rm -rf /var/cache/* \
    && mkdir /var/cache/apk

ENV CHROME_BIN=/usr/bin/chromium-browser \
    CHROME_PATH=/usr/lib/chromium/

# create gitlab runner user and add it to node group
RUN usermod -aG 999 node

# change owner of /home/node to user node group node
RUN chown -R node:node /home/node

# make owner and group permission to write and execute node home directory
RUN chmod -R 774 /home/node

RUN git config --global --add safe.directory '*'

USER node

SHELL ["/bin/bash", "-c"]